

var app4 = new Vue({
  el:".app-4",
	data:{
		allPrice:0,
		products: {
			tort:{
				label:'Торт',
				maxLayers:3,
				amount: 1
			},
			cake:{
				label:'Кекс',
				maxLayers:1,
				amount: 1
			},
			Pop:{
				label:'Попс',
				maxLayers:3,
				amount: 1
			},
		},
		viewsTort:{
			classic:{
				label:"Классический",
				products:{
					nevejskii:{
						label:'Невежеский',
						price: 2500,
						image:'img/torts/tort.jpg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
					city:{
						label:'Городской',
						price: 800,
						image:'img/torts/tort2.jpeg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
					kurgan:{
						label:'Курганский',
						price: 500,
						image:'img/torts/tort3.jpeg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
					lights:{
						label:'Патриот',
						price: 1500,
						image:'img/torts/tort4.jpeg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
				}
			},
			cccr:{
				label:"ссср",
				products:{
					nevejskii:{
						label:'Невежеский',
						price: 2500,
						image:'img/torts/tort.jpg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
					city:{
						label:'Городской',
						price: 800,
						image:'img/torts/tort2.jpeg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
					kurgan:{
						label:'Курганский',
						price: 500,
						image:'img/torts/tort3.jpeg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
					lights:{
						label:'Патриот',
						price: 1500,
						image:'img/torts/tort4.jpeg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
				}
			},
			nabis:{
				label:"На Бис",
				products:{
					nevejskii:{
						label:'Невежеский',
						price: 2500,
						image:'img/torts/tort.jpg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
					city:{
						label:'Городской',
						price: 800,
						image:'img/torts/tort2.jpeg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
					kurgan:{
						label:'Курганский',
						price: 500,
						image:'img/torts/tort3.jpeg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
					lights:{
						label:'Патриот',
						price: 1500,
						image:'img/torts/tort4.jpeg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
					lightsds:{
						label:'Патриот',
						price: 1500,
						image:'img/torts/tort4.jpeg',
						number: false,
						description:"Песочный полуфабрикат, суфле,"+ 
						"крем из растительный сливок,"
						 +"шоколадная глазурь, джем черная смородина"
					},
				},
			},
			custom:{
				label:"Сделать свой!",
			},
		},
		content:false,
		customNameTort:'',
		inputTextName:"",	
		faq: false,
		textFaq: true,
		teamFaq: true,
		displayFaqText:'',
		displayFaqTeam:'',
		displayBlock:'none',
		displayButtonShowTeam:true,
		displayButtonShowText: false,
		descCalculater : [],
			layers : [
				{
					height:10,
					type:"biscuit"
				},
				{
					height:10,
					type:"jele"
				},
				{
					height:10,
					type:"biscuit"
				},
			],
			marginTopForSweetness:-30,
			
			layersTypes:{
				
				biscuit:{
					price1sm: 50,
					label: 'Бисквит'
				},
				beze:{
					price1sm: 50,
					label: 'Безе'
				},
				curd:{
					price1sm: 60,
					label: 'Твороженнный'
				},
				jele:{
					price1sm: 60,
					label: 'Желе'
				},
			},
			defaultLayer : "biscuit",
			defaultHeight : "10",
			maxAmountLayers: 3,
			maxHeight:30,
			cakeForms: {
				circle:{
					radius: 50,
					label: "Круглый"
				},
				square:{
					radius: 10,
					label: "Квадратный"
				},
			},
			radiusForm : 0,
			valueSelectedForm:'',
			cakeForms:{
				circle: "Круг",
				squired: "Квадрат"
		},
		selectedForm:"squired",
		selectedProductOrder: "Вы ничего не выбрали!",
		selectedProduct:"Торт",
		selectedViewTort:'Сделать свой!',
		imageSelectedProduct: 'img/torts/tort4.jpeg',
		imageSelectedTort:"img/torts/none.png",
		photoOfCatalog:false,
		formWasSelected: false,
		contsValue : 20,
		aOnwidget: 'https://ru.tradingview.com/symbols/NASDAQ-AAPL/',
		aOnScript: 'https://s3.tradingview.com/external-embedding/embed-widget-symbol-info.js',
		items:[
			"home",
			"FAQ",
			"contact",
			"map"
		],
		show:false,
		},
		
	computed:{
		price(){
			let sum = 0;

			this.layers.forEach((layer)=>{
				sum = sum + layer.height * this.layersTypes[layer.type].price1sm;
			});

			return sum;
		},
		correctHeight(){

			this.layers.forEach((layer)=>{
				if(layer.height> this.maxHeight){
					layer.height=this.maxHeight;
					alert('Максимальная длина одного слоя: ' + this.maxHeight);
				};
				
				
				
			});
		},
		
		
		
		
		
		
	},
	methods:{
		addLayer(){
			app4.layers.unshift({ 
				type: this.defaultLayer,
				height: this.defaultHeight
			 });
			 this.i++;
			 this.marginTopForSweetness -= this.contsValue;
		},
		deleteLayer(i){
			this.layers.splice(i, 1);
			this.marginTopForSweetness += this.contsValue;
		},
		alertText(text){
			alert(text);
			
		},
		setRadius(cakeForm){
			this.radiusForm = cakeForm.radius;
		},
		showTeamHideText(){
			this.displayFaqText = 'none';
			this.displayFaqTeam = 'block';
			this.displayButtonShowTeam = false;
			this.displayButtonShowText =true;

		},
		showTextHideTeam(){
			this.displayFaqText = 'block';
			this.displayFaqTeam = 'none';
			this.displayButtonShowTeam = true;
			this.displayButtonShowText =false;

		},
		toOrder(priceTort, selectedProduct, imageProduct){
			this.allPrice = priceTort;
			this.selectedProductOrder = selectedProduct;
			this.photoOfCatalog =true;
			this.imageSelectedProduct = imageProduct;
		},
		toOrderCustom(priceTort){
			this.allPrice = priceTort;
			this.photoOfCatalog =false;
			this.selectedProductOrder = "Вы создали свой собственный торт!"
		},
		nameTort(nameTort){
			this.customNameTort = nameTort;
		},
		chooseCakeForm(){
			this.formWasSelected = true;
		},
		chooseBlock1(){
			this.customNameTort =''
		},
		chooseBlock2(){
			this.formWasSelected =false
		},
		alertText(){
			this.customNameTort =''
		}
		
	}
})


